import React, { FC, useState, ChangeEvent, useCallback, useEffect } from 'react';
import { Container } from '../../components/Container';
import { Button } from '../../components/Button';
import { Field } from '../../components/Field';
import { Link } from 'react-router-dom';
import { TextInteractive } from '../../components/TextInteractive';
import { apiConnect, apiErrors, connectionError } from '../../core';
import { FormResponse } from '../../components/FormResponse';
import { useHistory } from 'react-router-dom';
import { useAction, useAtom } from '@reatom/react';
import { authAtom, authRespond } from '../../store/atoms/authAtom';
import { isAuth } from '../../core/helpers';

export const Signup: FC = () => {
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  const handleChange = (setter: (newState: string) => void) => (event: ChangeEvent<HTMLInputElement>) => {
    event.target ? setter(event.target.value) : void 0;
  }

  const formResponse = useAtom(authAtom);
  const setFormResponse = useAction(payload => authRespond(payload));

  useEffect(() => {
    isAuth && history.push("/");
  }, [history]);

  const register = useCallback((e) => {
    e.preventDefault();
    apiConnect.post(`/user/register?email=${email}&name=${name}&password=${password}&password_confirm=${passwordConfirmation}&phone=${phone}`)
      .then(response => {
        if(response.data.error) {
          setFormResponse(apiErrors[response.data.error.id] || response.data.error.message);
        }
        else {
          setFormResponse(`Добро пожаловать, ${response.data.body.email}!`);
          localStorage.access_token = response.data.body.token;
          localStorage.email = response.data.body.email;
          history.go(0);
        }
      })
      .catch(error => setFormResponse(connectionError))
  }, [setFormResponse, email, name, password, passwordConfirmation, history]);


  return (
    <div className="py-5 animate__animated animate__backInUp">
      <Container className="py-5 text-center" mini>
        <h2>Зарегистрироваться</h2>
        {formResponse.length > 0 && <FormResponse className="mb-2 animate__animated animate__bounceIn">{formResponse}</FormResponse>}
        <form onSubmit={register}>
          <Field className="d-block w-100 p-3 mb-2" type="text" placeholder="Электронная почта" onChange={handleChange(setEmail)} />
          <Field className="d-block w-100 p-3 mb-2" type="text" placeholder="Имя, Фамилия, Отчество" onChange={handleChange(setName)} />
          <Field className="d-block w-100 p-3 mb-2" type="text" placeholder="Ваш номер телефона" onChange={handleChange(setPhone)} />
          <Field className="d-block w-100 p-3 mb-2" type="password" placeholder="Пароль" onChange={handleChange(setPassword)} />
          <Field className="d-block w-100 p-3 mb-2" type="password" placeholder="Подтверждение пароля" onChange={handleChange(setPasswordConfirmation)} />
          <Button className="d-block w-100 mt-3" onClick={register}>Зарегистрироваться</Button>
        </form>
        <p>Уже зарегистрированы?
          <Link className="ml-1" to="/login">
            <TextInteractive>Войти</TextInteractive>
          </Link>
        </p>
      </Container>
    </div>
  );
}
