import React, { FC, useEffect, useState } from 'react';
import { apiConnect, Order } from '../../core';
import { isAuth } from '../../core/helpers';
import { Container } from '../../components/Container';
import { OrderCard } from '../../components/organisms/OrderCard';
import { useHistory } from 'react-router-dom';

export const Driver: FC = () => {
  const history = useHistory();
  const [orderList, setOrderList] = useState<ReadonlyArray<Order>>([]);
  const reload = () => {
    history.go(0);
  }
  
  useEffect(() => {
    

    apiConnect.get(`order/list?token=${isAuth}`)
      .then(response => {
        if(response.data.body.orders){
          setOrderList(response.data.body.orders);
        }
      })
    if(isAuth){
      apiConnect.get(`user/get/token?token=${isAuth}`)
        .then(response => {
          if(!response.data.body || response.data.body.user.is_driver == "0"){
            history.push('/');
          }
        })
    }
    setTimeout(reload, 60000);
  }, [setOrderList]);

  return (
    <Container className="px-3" >
      <h2>Список заказов</h2>
      {orderList.map((n, i) => <OrderCard className="mb-2 animate__animated animate__fadeIn" order={n} key={i} />)}
      {orderList.length == 0 && <span>В настоящий момент нет заказов</span>}
    </Container>
  );
}
