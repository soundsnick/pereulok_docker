/* @jsx jsx */
import { jsx } from "@emotion/core";
import { useAction, useAtom } from "@reatom/react";
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Button } from "../../components/Button";
import { Container } from '../../components/Container';
import { Field } from "../../components/Field";
import { FormResponse } from "../../components/FormResponse";
import { apiConnect } from "../../core";
import { handleNumberInputChange, isAuth } from '../../core/helpers';
import { User } from "../../core/types/User";
import { authAtom, authRespond } from "../../store/atoms/authAtom";

export const BalanceFailure: FC = () => {
    const history = useHistory();
    const [user, setUser] = useState<User>();
    const useQuery = () => new URLSearchParams(useLocation().search);

    const queries = useQuery();
    const orderId = queries.get('pg_order_id');

    useEffect(() => {
        isAuth || history.push("/");
        if(isAuth){
            apiConnect.get(`user/get/token?token=${isAuth}`)
                .then(response => {
                    if(!response.data.body || response.data.body.user){
                        setUser(response.data.body.user);
                    }
                });
        }
        if (orderId) {
          apiConnect.post(`/payment/failure`, { order_id: orderId })
            .then(() => {
              console.log("PAYMENT FAILURE!");
              setTimeout(() => {
                history.push('/profile');
              }, 5000)
            })
            .catch(() => {
              console.log("SERVER ERROR!");
            });
        } else {
          history.push("/")
        }
    }, [history]);

    return (
        <div className="animate__animated animate__fadeIn">
            <Container mini className="text-center">
              <img src="/error.png" css={{ maxWidth: 200, paddingTop: 150 }} alt=""/>
              <h1 css={{ paddingTop: 10, textAlign: 'center', lineHeight: 1.25 }}>Ошибка платежа. Попробуйте позже</h1>
            </Container>
        </div>
    )
}
