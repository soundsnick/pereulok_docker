import React, { FC, useEffect } from 'react';
import { pipe } from 'fp-ts/pipeable';
import { isAuth } from '../../core/helpers';
import { apiConnect } from '../../core';
import { useHistory } from 'react-router-dom';

export const Logout: FC = () => {
  const history = useHistory();
  useEffect(() => {
    isAuth ?
    pipe(
      history,
      () => apiConnect.get(`/user/logout?token=${isAuth}`),
      () => localStorage.removeItem('access_token'),
      () => localStorage.removeItem('email'),
      () => history.go(0)
    ) : history.push("/login")
  }, [history]);
  return (<div>Вы вышли из аккаунта</div>)
};
