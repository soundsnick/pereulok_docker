/* @jsx jsx */
import { jsx } from "@emotion/core";
import { useAction, useAtom } from "@reatom/react";
import React, { FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from "../../components/Button";
import { Container } from '../../components/Container';
import { Field } from "../../components/Field";
import { FormResponse } from "../../components/FormResponse";
import { apiConnect, payboxConnect } from '../../core';
import { handleNumberInputChange, isAuth } from '../../core/helpers';
import { User } from "../../core/types/User";
import { authAtom, authRespond } from "../../store/atoms/authAtom";
import { Payment, PAYMENT_DEFAULT, PaymentRequest } from '../../core/payment';
import md5 from 'md5';
import { marshalToUrl } from '../../core/marshalToUrl';

export const Balance: FC = () => {
    const history = useHistory();
    const [user, setUser] = useState<User>();
    const [balance, setBalance] = useState<number>(500);

    useEffect(() => {
        isAuth || history.push("/");
        if(isAuth){
            apiConnect.get(`user/get/token?token=${isAuth}`)
                .then(response => {
                    if(!response.data.body || response.data.body.user){
                        setUser(response.data.body.user);
                    }
                });
        }
    }, [history]);

    const formResponse = useAtom(authAtom);
    const setFormResponse = useAction(payload => authRespond(payload));

    // @ts-ignore
    const genSignature = (payment: Payment) => md5(`init_payment.php;` + Object.keys(payment).sort().map(n => payment[n]).join(';') + `;ubzWnM8VXHi7AtgZ`);

    const handleBalance = async () => {
      if(balance < 500) {
        setFormResponse('Минимальная сумма пополнения 500тг');
      } else {
        const payment: Payment = {
          ...PAYMENT_DEFAULT,
          pg_amount: balance,
          pg_salt: md5(balance+PAYMENT_DEFAULT.pg_merchant_id+(user?.email || '')),
          pg_order_id: md5((user?.email || "")+balance+PAYMENT_DEFAULT.pg_merchant_id),
        }
        const paymentProcess: PaymentRequest = {
          ...payment,
          pg_sig: genSignature(payment)
        }
        await apiConnect.post(`/payment/create`, { user_id: user?.id, order_id: payment.pg_order_id, amount: balance, paybox: marshalToUrl(paymentProcess) })
          .then(res => {
            if (res.data?.redirect_url) {
              window.location.href = res.data.redirect_url;
            }
          });
      }
    }

    return (
        <div className="animate__animated animate__fadeIn">
            <Container mini>
                <h1 css={{ paddingTop: 50 }}>Пополнить баланс</h1>
                {formResponse.length > 0 && <FormResponse className="mb-4 animate__animated animate__bounceIn">{formResponse}</FormResponse>}
                <span css={{ display: "block", marginBottom: 7 }}>Введите сумму</span>
                <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder="Введите сумму для пополнения" onChange={handleNumberInputChange(setBalance)}/>
                <Button className="d-block w-100 mt-3" onClick={handleBalance}>Пополнить</Button>
            </Container>
        </div>
    )
}
