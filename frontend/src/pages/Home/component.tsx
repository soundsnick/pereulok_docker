/** @jsx jsx */
import { jsx } from '@emotion/core';
import { Fragment, useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { GeoObjectProps, Map, Placemark, YMaps, YMapsApi, YMapsProps } from 'react-yandex-maps';
import { OrderForm } from '../../components/organisms/OrderForm';
import { isAuth } from '../../core/helpers';
import { useAction, useAtom } from '@reatom/react';
import { setToCoordsAction, toCoordsAtom } from '../../store/atoms/toCoordsAtom';
import { fromCoordsAtom, setFromCoordsAction } from '../../store/atoms/fromCoordsAtom';
import { pipe } from 'fp-ts/pipeable';
import { setToAddressAction } from '../../store/atoms/toAddressAtom';
import { setFromAddressAction } from '../../store/atoms/fromAddressAtom';
import { choiceAtom, setChoiceAction } from '../../store/atoms/choiceAtom';
import { apiConnect, Colors } from '../../core';
import { User } from '../../core/types/User';
import mapImg from '../../assets/mapImg.png';

export const Home = () => {
  const history = useHistory();
  const choice = useAtom(choiceAtom);
  const setChoice = useAction(setChoiceAction);

  const toCoords = useAtom(toCoordsAtom);
  const setToCoords = useAction(setToCoordsAction);
  const fromCoords = useAtom(fromCoordsAtom);
  const setFromCoords = useAction(setFromCoordsAction);

  const setFromAddress = useAction(setFromAddressAction);
  const setToAddress = useAction(setToAddressAction);

  const [mapInstance, setMapInstance] = useState<YMapsApi>();
  const [user, setUser] = useState<User>({});
  const [isOpen, setIsOpen] = useState(true);

  const moveMarker = useCallback((e: YMapsProps) => {
    if(!localStorage.current_order){
      const coords = e.get('coords');
      choice ? setToCoords(coords) : setFromCoords(coords);
      if (mapInstance) {
        mapInstance.geocode(coords)
          .then(
            (res: GeoObjectProps<any>) => pipe(
              res.geoObjects.get(0),
              (geoObj) => geoObj.getAddressLine(),
              (address) => choice ? setToAddress(address) : setFromAddress(address),
              () => setChoice(choice)
            )
          );
      }
    }
  }, [mapInstance, setFromCoords, setToCoords, setChoice, choice, setFromAddress, setToAddress]);

  const initMapInstance = (instance: YMapsApi) => {
    setMapInstance(instance);
  }

  useEffect(() => {
    isAuth || history.push("/welcome");
    if(isAuth){
      apiConnect.get(`user/get/token?token=${isAuth}`)
        .then(response => {
          if(response.data.body){
            setUser(response.data.body.user);
          }
        })
    }
  }, [history]);

  return (
    <Fragment>
      <div className="order-form" css={{ maxWidth: 600, position: "fixed", left: 10, zIndex: 9999, bottom: 0, transform: `translateY(${isOpen ? 0 : "600px"})`, transition: "transform 0.2s" }}>
        <button 
          className="close-button" 
          onClick={() => setIsOpen(!isOpen)}
          css={{
            display: "block",
            margin: "auto",
            boxShadow: "0 -1px 20px rgba(0,0,0,.3)",
            border: 0,
            padding: "10px 35px",
            borderRadius: "5px 5px 0 0",
            fontFamily: "inherit",
            fontSize: 14,
            background: "#fdd641",
            fontWeight: 500,
            color: "#282828",
            cursor: "pointer",
            outline: "none",
          }}
        >{isOpen ? "Свернуть" : "Развернуть"}</button>
        <OrderForm className="p-2" />
      </div>
      {user.is_driver == 0 && localStorage.current_order ?
        <img className="animate__animated animate__fadeIn" css={{ width: "100%", filter: "blur(3px)" }} src={mapImg} /> :
        <YMaps
          classNames="w-100 h-100"
          query={{
            ns: "use-load-option",
            apikey: "9b71dbc6-0f5c-4cfa-85fe-12584a8e8204",
            load: "geocode"
          }}
        >
          <Map
            defaultState={{ center: [43.240818, 76.9], zoom: 17 }}
            width="100%"
            height="100vh"
            onClick={moveMarker}
            onLoad={initMapInstance}
          >
            {fromCoords[0] > 0 && <Placemark geometry={fromCoords} options={{ iconColor: Colors.PrimaryDark }} />}
            {toCoords[0] > 0 && <Placemark geometry={toCoords} options={{ iconColor: Colors.PrimaryDark }} hint="B" />}
          </Map>
        </YMaps>
      }
    </Fragment>
  );
}
