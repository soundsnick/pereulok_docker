import React, { FC, useState, useCallback, useEffect } from 'react';
import { Container } from '../../components/Container';
import { Button } from '../../components/Button';
import { Field } from '../../components/Field';
import { Link } from 'react-router-dom';
import { TextInteractive } from '../../components/TextInteractive';
import { apiConnect, apiErrors, connectionError } from '../../core';
import { useHistory } from 'react-router-dom';
import { FormResponse } from '../../components/FormResponse';
import { useAction, useAtom } from '@reatom/react';
import { authAtom, authRespond } from '../../store/atoms/authAtom';
import { handleInputChange, isAuth } from '../../core/helpers';

export const Login: FC = () => {
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const formResponse = useAtom(authAtom);
  const setFormResponse = useAction(payload => authRespond(payload));

  useEffect(() => {
    isAuth && history.push("/");
  }, [history]);

  const login = useCallback((e) => {
    e.preventDefault();
    apiConnect.post(`/user/login?email=${email}&password=${password}`)
      .then(response => {
        if(response.data.error)
          setFormResponse(apiErrors[response.data.error.id] || response.data.error.message);
        else{
          setFormResponse('Добро пожаловать!')
          localStorage.access_token = response.data.body.token
          localStorage.email = response.data.body.email
          history.go(0);
        }
      })
      .catch(error => setFormResponse(connectionError));
  }, [email, password, history, setFormResponse]);

  return (
    <div className="py-5 animate__animated animate__backInUp">
      <Container className="py-5 text-center" mini>
        <h2>Войти в аккаунт</h2>
        {formResponse.length > 0 && <FormResponse className="mb-2 animate__animated animate__bounceIn">{formResponse}</FormResponse>}
        <form onSubmit={login}>
          <Field className="d-block w-100 p-3 mb-2" type="text" placeholder="Электронная почта" onChange={handleInputChange(setEmail)} />
          <Field className="d-block w-100 p-3 mb-2" type="password" placeholder="Пароль" onChange={handleInputChange(setPassword)} />
          <Button className="d-block w-100 mt-3" onClick={login}>Войти</Button>
        </form>
        <p>Вы здесь впервые?
          <Link className="ml-1" to="/signup">
            <TextInteractive>Зарегистрироваться</TextInteractive>
          </Link>
        </p>
      </Container>
    </div>
  );
}
