/* @jsx jsx */
import { jsx } from "@emotion/core";
import { useAction, useAtom } from "@reatom/react";
import React, { FC, useEffect, Fragment, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from "../../components/Button";
import { Container } from '../../components/Container';
import { Field } from "../../components/Field";
import { FormResponse } from "../../components/FormResponse";
import { apiConnect } from "../../core";
import { handleInputChange, isAuth } from '../../core/helpers';
import { User } from "../../core/types/User";
import { authAtom, authRespond } from "../../store/atoms/authAtom";

export const Profile: FC = () => {
    const history = useHistory();
    const [user, setUser] = useState<User>();
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [car, setCar] = useState('');
    const [carNumber, setCarNumber] = useState('');

    useEffect(() => {
        isAuth || history.push("/");
        if(isAuth){
            apiConnect.get(`user/get/token?token=${isAuth}`)
                .then(response => {
                    if(!response.data.body || response.data.body.user){
                        setUser(response.data.body.user);
                    }
                });
        }
    }, [history]);

    const formResponse = useAtom(authAtom);
    const setFormResponse = useAction(payload => authRespond(payload));



    const update = () => {
        const query = user?.is_driver ? `user/update?token=${isAuth}&name=${name.length > 0 ? name : user?.name}&phone=${phone.length > 0 ? phone : user?.phone}&car=${car.length > 0 ? car : user?.car}&car_number=${carNumber.length > 0 ? carNumber : user?.car_number}` : `user/update?token=${isAuth}&name=${name.length > 0 ? name : user?.name}&phone=${phone.length > 0 ? phone : user?.phone}`
        apiConnect.post(query)
            .then(response => {
                setFormResponse('Сохранено!')
            })
    }

    return (
        <div className="animate__animated animate__fadeIn">
            <Container mini>
                <h1 css={{ paddingTop: 50 }}>Профиль {user?.name || ""}</h1>
                <div className="d-flex align-items-center justify-content-between pb-5">
                  <h3>Баланс: {user?.balance || 0}тг</h3>
                  <Button onClick={() => history.push("/balance")}>Пополнить баланс</Button>
                </div>
                {formResponse.length > 0 && <FormResponse className="mb-4 animate__animated animate__bounceIn">{formResponse}</FormResponse>}
                <span css={{ display: "block", marginBottom: 7 }}>Ваша почта (нельзя изменить)</span>
                <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder="Ваш адрес электронной почты" value={user?.email} disabled />
                <span css={{ display: "block", marginBottom: 7 }}>Изменить имя</span>
                <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder={`Ваше имя ${user?.name && '('+user.name+')'}`} onChange={handleInputChange(setName)} />
                <span css={{ display: "block", marginBottom: 7 }}>Изменить номер телефона</span>
                <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder={`Ваш номер телефона ${user?.phone && '('+user.phone+')' || ''} `} onChange={handleInputChange(setPhone)} />
                {user?.is_driver ?
                    <Fragment>
                        <span css={{ display: "block", marginBottom: 7 }}>Изменить модель машины</span>
                        <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder={`Ваша модель машины ${user?.car && '('+user.car+')' || ''} `} onChange={handleInputChange(setCar)} />
                        <span css={{ display: "block", marginBottom: 7 }}>Изменить номер машины</span>
                        <Field css={{ padding: 20, width: "100%", marginBottom: 25 }} placeholder={`Ваш номер машины ${user?.car_number && '('+user.car_number+')' || ''} `} onChange={handleInputChange(setCarNumber)} />
                    </Fragment>
                : <Fragment></Fragment>}
                <Button className="d-block w-100 mt-3" onClick={update}>Сохранить</Button>
            </Container>
        </div>
    )
}
