/* @jsx jsx */
import { jsx } from '@emotion/core';
import { url } from 'inspector';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from '../../components/Button';
import { Container } from '../../components/Container';

export const Welcome = () => {
    const router = useHistory();
    return (
        <div
            css={{
                backgroundImage: 'linear-gradient(to top, rgb(255 229 129 / 37%), rgba(0,0,0,.8)), url("/bg.jpg")',
                height: "100vh",
                backgroundSize: "cover",
                paddingTop: 250,
            }}
        >
            <Container className="animate__animated animate__bounceIn" mini css={{  background: "#fff", padding: 20 }}>
                <h1 css={{ textAlign: "center", marginBottom: 5, lineHeight: "1.3" }}>Добро пожаловать в Jolai!</h1>
                <p css={{ fontSize: 15, lineHeight: 1.4, textAlign: "center", marginBottom: 40, marginTop: 0 }}>Поездки внутри и вне города. Разная техника к вашим услугам.</p>
                <Button className="ml-2 mb-2 w-100 py-3" css={{ fontSize: 16 }} onClick={() => router.push('/signup')}>Стать пассажиром</Button>
                <Button className="ml-2 mb-2 w-100 py-3" css={{ fontSize: 16 }} onClick={() => router.push('/signup-driver')}>Стать водителем</Button>
            </Container>
        </div>
    )
};