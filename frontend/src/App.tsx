/** @jsx jsx */
import { jsx } from '@emotion/core';
import {
  Switch,
  Route,
  BrowserRouter as Router,
} from "react-router-dom";
import { Home } from './pages/Home';
import { Welcome } from './pages/Welcome';
import { Header } from './components/Header';
import { Login } from './pages/Login';
import { Signup } from './pages/Signup';
import { SignupDriver } from './pages/SignupDriver';
import { context } from '@reatom/react';
import { createStore } from '@reatom/core';
import { Logout } from './pages/Logout';
import { Driver } from './pages/Driver';
import 'animate.css';
import { Profile } from './pages/Profile';
import { Balance } from './pages/Balance';
import { BalanceSuccess } from './pages/BalanceSuccess';
import { BalanceFailure } from './pages/BalanceFailure';


const { Provider } = context;

function App() {
  const store = createStore();

  return (
    <Provider value={store}>
      <div className="App">
        <Router>
          <Header css={{ top: 0, left: 0, width: "100%", zIndex: 9999 }} />
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/signup">
              <Signup />
            </Route>
            <Route path="/signup-driver">
              <SignupDriver />
            </Route>
            <Route path="/logout">
              <Logout />
            </Route>
            <Route path="/driver">
              <Driver />
            </Route>
            <Route path="/welcome">
              <Welcome />
            </Route>
            <Route path="/profile">
              <Profile />
            </Route>
            <Route path="/balance">
              <Balance />
            </Route>
            <Route path="/balance-success">
              <BalanceSuccess />
            </Route>
            <Route path="/balance-failure">
              <BalanceFailure />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
