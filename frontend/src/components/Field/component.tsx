import styled from '@emotion/styled';
import { Colors } from '../../core';
import { Props } from './props';
import { css } from '@emotion/core';
import { rgba } from 'emotion-rgba/dist';

export const Field = styled.input<Props>`
  display: block;
  border-radius: 4px;
  border: 0;
  background: ${Colors.Grey};
  font-family: inherit;
  font-size: 13px;
  font-weight: 400;
  ${({ active }: Props) => active &&
    css`
      border: 2px solid ${Colors.PrimaryDarker};
      background: ${rgba(Colors.Primary, 0.2)};
    `
  };
`;
