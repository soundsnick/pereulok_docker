/* @jsx jsx */
import { jsx } from "@emotion/core";
import React, { FC, Fragment, HTMLAttributes, useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import logo from '../../assets/logo_two.png';
import styled from '@emotion/styled';
import classNames from 'classnames';
import { NavigationItem } from './libs/NavigationItem';
import { isAuth } from '../../core/helpers';
import { User } from '../../core/types/User';
import { apiConnect } from '../../core';

const Logo = styled.img`
  height: 40px;
  border-radius: 10px;
  margin: 5px 0;
`

const HeaderBase: FC<HTMLAttributes<HTMLDivElement>> = ({ className, ...rest }: HTMLAttributes<HTMLDivElement>) => {
  const location = useLocation();
  const [user, setUser] = useState<User>();

  useEffect(() => {
    if(isAuth){
      apiConnect.get(`user/get/token?token=${isAuth}`)
        .then(response => {
          if(response.data.body){
            setUser(response.data.body.user);
          }
        })
    }
  }, [setUser])

  return (
    <div className={classNames(className, "d-flex", "p-1 px-3", "justify-content-between", "align-items-center", { "position-fixed": location.pathname === "/" }, 'animate__animated animate__bounceInDown')} {...rest}>
      <Link className="d-flex align-items-center" to="/">
        <Logo src={logo} alt="" />
        <h1 css={{ margin: 0, marginLeft: 10 }}>Jolai</h1>
      </Link>
      <nav>
        {isAuth ?
          <Fragment>
            {user && user.is_driver ? (
              <Link to="/driver">
                <NavigationItem className="px-2">Список заказов</NavigationItem>
              </Link>
            ) : <Fragment></Fragment>}
            <Link to="/profile">
                <NavigationItem className="px-2">Профиль</NavigationItem>
              </Link>
            <Link to="/logout">
              <NavigationItem className="px-2">Выйти</NavigationItem>
            </Link>
          </Fragment>
          :
          <Fragment>
            <Link to="/signup">
              <NavigationItem className="px-2">Регистрация</NavigationItem>
            </Link>
            <Link to="/login">
              <NavigationItem className="px-2">Войти</NavigationItem>
            </Link>
          </Fragment>
        }
      </nav>
    </div>
  );
}

export const Header = styled(HeaderBase)`
  background: #fff;
  box-shadow: 1px 0 2px rgba(0,0,0,.2);
`;
