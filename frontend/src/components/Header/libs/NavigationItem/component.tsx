import styled from '@emotion/styled';
import { Colors } from '../../../../core';

export const NavigationItem = styled.span`
  transition: color 0.1s;
  font-size: 14px;
  font-weight: 500;
  &:hover {
    color: ${Colors.PrimaryDark};
  }
`;
