/** @jsx jsx */
import { jsx } from '@emotion/core';
import { FC, Fragment, useEffect, useState } from 'react';
import styled from '@emotion/styled';
import { Props } from './props';
import { Field } from '../../Field';
import { Button } from '../../Button';
import { formatDate, handleInputChange, isAuth } from '../../../core/helpers';
import { useAction, useAtom } from '@reatom/react';
import { setToCoordsAction, toCoordsAtom } from '../../../store/atoms/toCoordsAtom';
import { fromCoordsAtom, setFromCoordsAction } from '../../../store/atoms/fromCoordsAtom';
import { setToAddressAction, toAddressAtom } from '../../../store/atoms/toAddressAtom';
import { fromAddressAtom, setFromAddressAction } from '../../../store/atoms/fromAddressAtom';
import { choiceAtom } from '../../../store/atoms/choiceAtom';
import { apiConnect, OrderProcess } from '../../../core';
import { Order } from '../../../core';
import { pipe } from 'fp-ts/pipeable';
import { useHistory } from 'react-router-dom';
import { User } from '../../../core/types/User';
import { Tariff } from './types/Tariff';

const OrderFormBase: FC<Props> = ({ ...rest }: Props) => {
  const toAddress = useAtom(toAddressAtom);
  const setToAddress = useAction(setToAddressAction);

  const fromAddress = useAtom(fromAddressAtom);
  const setFromAddress = useAction(setFromAddressAction);

  const [cost, setCost] = useState('300');
  const toCoords = useAtom(toCoordsAtom);
  const fromCoords = useAtom(fromCoordsAtom);
  const setToCoords = useAction(setToCoordsAction);
  const setFromCoords = useAction(setFromCoordsAction);
  const history = useHistory();
  const [tariffs, setTariffs] = useState<Tariff[]>();
  const [checkedTariffs, setCheckedTariffs] = useState<number[]>([]);

  const query = {
    token: isAuth,
    cost: cost,
    to_address: toAddress,
    from_address: fromAddress,
    from_coords: fromCoords.join(','),
    to_coords: toCoords.join(','),
    tariffs: checkedTariffs.join(',')
  }

  const resetCoords = () => {
    setToCoords([0, 0]);
    setFromCoords([0, 0]);
  }

  const choice = useAtom(choiceAtom);

  const createOrder = () => {
    apiConnect.post('/order/create', query)
      .then(response => {
        if(response.data.body){
          localStorage.current_order = JSON.stringify(response.data.body);
          setCurrentOrder(response.data.body.order);
          setFromCoords(response.data.body.order.from_coords.split(','));
          setToCoords(response.data.body.order.to_coords.split(','));
          reload();
        }
      })
  }

  const cancelOrder = () => {
    apiConnect.post('/order/cancel', { token: isAuth })
      .then(response => {
        if(response.data.body && response.data.body.message === "deleted"){
          localStorage.removeItem("current_order");
          history.go(0);
        }
      })
  }

  const [currentOrder, setCurrentOrder] = useState<Order>({});
  const [currentOrderProcess, setCurrentOrderProcess] = useState<OrderProcess>({});
  const [user, setUser] = useState<User>({});
  const [driver, setDriver] = useState<User>({});
  const reload = () => {
    history.go(0);
  }
  

  const handleTariffClick = (id: number) => () => {
    checkedTariffs.includes(id) ? setCheckedTariffs(checkedTariffs.filter(n => n != id)) : setCheckedTariffs([...checkedTariffs, id])
  }

  useEffect(() => {
    apiConnect.get('/tariff/list')
      .then(response => {
        setTariffs(response.data.body.tariffs)
      })

    if(localStorage.current_order) {
      const tmp_res = JSON.parse(localStorage.current_order);
      const tmp_order = tmp_res.order;
      checkOrder(tmp_order);

      setCurrentOrder(tmp_order);
      setFromCoords(tmp_order.from_coords.split(','));
      setToCoords(tmp_order.to_coords.split(','));
      if(tmp_res.process) {

        const tmp_order_process = tmp_res.process;
        setCurrentOrderProcess(tmp_order_process);

      }
    }
    else {
      checkOrderUser();
    }
    if(isAuth){
      apiConnect.get(`user/get/token?token=${isAuth}`)
        .then(response => {
          if(response.data.body){
            console.log(response.data.body.user);
            setUser(response.data.body.user);
          }
        })
    }

  }, [setCurrentOrder, setCurrentOrderProcess, setTariffs])

  useEffect(() => {
    if(user.is_driver == 0 && currentOrder.id)
      setTimeout(reload, 10000);
  }, [user]);

  const finishOrder = () => {
    apiConnect.get(`driver/finish?token=${isAuth}&order_id=${currentOrder.id}`)
      .then(response => {
        if(response.data.body && response.data.body.order){
          localStorage.removeItem("current_order");
          history.push('/driver');
        }
      })
  };

  const checkOrder = (order: Record<any, any>) => {
    apiConnect.get(`order/get?token=${isAuth}&order_id=${order.id}`)
      .then(response => {
        if(response.data.body){
          if(response.data.body.order.finished == "1") {
            localStorage.removeItem("current_order");
          }else {
            if(response.data.body.process){
              setCurrentOrder(response.data.body.order);
              setFromCoords(response.data.body.order.from_coords.split(','));
              setToCoords(response.data.body.order.to_coords.split(','));
              setCurrentOrderProcess(response.data.body.process);
              apiConnect.get(`user/get/id?id=${response.data.body.process.user_id}`)
                .then(response => {
                  if(response.data.body){
                    setDriver(response.data.body.user);
                  }
                })
              localStorage.current_order = JSON.stringify(response.data.body);
            }
          }
        }else {
          localStorage.removeItem("current_order");
        }
      })
      .catch(err => console.log(err))
  }

  const checkOrderUser = () => {
    apiConnect.get(`order/get/user?token=${isAuth}`)
      .then(response => {
        if(response.data.body){
          console.log(response.data.body, isAuth)
          if(response.data.body.order.finished == "1") {
            localStorage.removeItem("current_order");
          }else {
            localStorage.current_order = JSON.stringify(response.data.body);
          }
        }
      })
      .catch(err => console.log(err))
  }


  return (
    <div {...rest}>

      {currentOrder.id ?
        (
          currentOrderProcess.order_id ?
          <Fragment>
            <h2 css={{ marginBottom: 15, textAlign: "center", fontSize: 20, fontWeight: 500 }}>{user.is_driver == 1 ? "Выполнение заказа..." : `Вас отвезет водитель ${driver.car}`}</h2>
            {user.is_driver != 1 && 
            <Fragment>
              <span
                css={{ 
                  display: "block",
                  textAlign: "center",
                  fontWeight: 500,
                  fontSize: 20,
                  marginBottom: 20
                 }}
              >{driver?.car_number}</span>
              <a href={`tel:${driver?.phone}`}
                css={{
                  marginBottom: 15,
                  display: "block",
                  width: "100%",
                  background: "#f7d140",
                  textAlign: "center",
                  padding: 20,
                  fontSize: 16,
                  fontWeight: 500,
                  borderRadius: 4
                }}
              >Позвонить водителю</a>
            </Fragment>}
            <Field type="text" className="d-block w-100 mb-2 p-3" placeholder="Откуда вас забрать?" value={currentOrder.from_address} disabled />
            <Field type="text" className="mb-2 d-block w-100 p-3" placeholder="Куда вы едете?" value={currentOrder.to_address} disabled />
            <div className="d-flex justify-content-between mt-4 mb-3" css={{ fontSize: 14, fontWeight: 500 }}>
              <span>Время заказа: </span>
              <span>
                {
                  currentOrder.order_time &&
                  pipe(
                    new Date(Date.parse(currentOrder.order_time)),
                    (date) => `${formatDate(date.getHours())}:${formatDate(date.getMinutes())}`
                  )
                }
              </span>
            </div>
            <div className="d-flex justify-content-between mb-3" css={{ fontSize: 14, fontWeight: 500 }}>
              <span>Время начала выполнения: </span>
              <span>
                {
                  currentOrderProcess.start_time &&
                  pipe(
                    new Date(Date.parse(currentOrderProcess.start_time)),
                    (date) => `${formatDate(date.getHours())}:${formatDate(date.getMinutes())}`
                  )
                }
              </span>
            </div>
            <div className="d-flex justify-content-between mt-2 mb-4" css={{ fontSize: 14, fontWeight: 500 }}>
              <span>Стоимость заказа: </span>
              <span>
                {currentOrder.cost} ₸
              </span>
            </div>
            {
              user.is_driver == 1 && <Button className="d-block w-100 mb-2" onClick={finishOrder}>Завершить заказ</Button>
            }
          </Fragment>
          :
          <Fragment>
            <h2 css={{ marginBottom: 15, textAlign: "center", fontSize: 20, fontWeight: 500 }}>Ожидание ответа от водителей...</h2>
            <Field type="text" className="d-block w-100 mb-2 p-3" placeholder="Откуда вас забрать?" value={currentOrder.from_address} disabled />
            <Field type="text" className="mb-2 d-block w-100 p-3" placeholder="Куда вы едете?" value={currentOrder.to_address} disabled />
            <div className="d-flex justify-content-between mt-4 mb-3" css={{ fontSize: 14, fontWeight: 500 }}>
              <span>Время заказа: </span>
              <span>
                {
                  currentOrder.order_time &&
                    pipe(
                      new Date(Date.parse(currentOrder.order_time)),
                      (date) => `${formatDate(date.getHours())}:${formatDate(date.getMinutes())}`
                    )
                }
              </span>
            </div>
            <div className="d-flex justify-content-between mt-2 mb-4" css={{ fontSize: 14, fontWeight: 500 }}>
              <span>Стоимость заказа: </span>
              <span>
                {currentOrder.cost} ₸
              </span>
            </div>
            <Button className="d-block w-100 mb-2" onClick={cancelOrder}>Отменить заказ</Button>
          </Fragment>
        ) :
        <Fragment>
          <h2 css={{ marginBottom: 15, textAlign: "center", fontSize: 20, fontWeight: 500 }}>Заказать такси</h2>
          <Field active={!choice} type="text" className="d-block w-100 mb-2 p-3" placeholder="Откуда вас забрать?" value={fromAddress} onChange={handleInputChange(setFromAddress, resetCoords)} />
          <Field active={choice} type="text" className="mb-2 d-block w-100 p-3" placeholder="Куда вы едете?" value={toAddress} onChange={handleInputChange(setToAddress, resetCoords)} />
          <span css={{ fontSize: 14, margin: "10px 0", marginTop: 20, display: "block" }}>Выберите категории:</span>
          <div css={{ display: "grid", gridTemplateColumns: "repeat(2, minmax(100px, 1fr))", gridGap: 8 }}>
            {tariffs && tariffs.map((n, i) => (
              <label key={i} css={{
                display: "block",
                padding: 10,
                background: "#ececec",
                borderRadius: 4,
                cursor: "pointer",
                transition: "background 0.2s, transform 0.2s",
                "&:hover": {
                  background: "#fef6dc",
                  transform: "scale(1.02)"
                },
                "&:active-within": {
                  background: "#fef6dc",
                  border: "2px solid #fcc901",
                }
              }}>
                <div className="d-flex align-items-center" onClick={handleTariffClick(n.id)}>
                  <input type="checkbox" value={n.id} />
                  <span className="d-block pl-2" css={{
                    fontSize: 15,
                    textTransform: "uppercase",
                    fontWeight: 500
                  }}>{n.name}</span>
                </div>
                <p css={{
                  marginTop: 5,
                  lineHeight: 1.3,
                  color: "grey"
                }}>{n.description}</p>
              </label>
            ))}
          </div>
          <span css={{ fontSize: 14, margin: "10px 0", marginTop: 20, display: "block" }}>Какую цену предлагаете таксисту? (₸)</span>
          <div className="d-flex">
            <Field className="d-block w-100 mb-2 p-3" type="number" min={300} value={cost} onChange={handleInputChange(setCost)} />
            <Button className="ml-2 mb-2" onClick={createOrder}>Заказать</Button>
          </div>
        </Fragment>
      }
    </div>
  );
}

export const OrderForm = styled(OrderFormBase)`
  background: #fff;
  width: 100%;
  border-radius: 10px 10px 0 0;
  box-shadow: 0 0 10px rgba(0,0,0,.2);
`;
