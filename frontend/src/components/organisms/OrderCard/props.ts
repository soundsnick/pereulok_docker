import { HTMLAttributes } from 'react';
import { Order } from '../../../core';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly order: Order;
}
