/** @jsx jsx */
import { jsx } from '@emotion/core';
import { FC, useEffect, useState } from 'react';
import { Props } from './props';
import styled from '@emotion/styled';
import { apiConnect, Colors } from '../../../core';
import classNames from 'classnames';
import { TextInteractive } from '../../TextInteractive';
import { Field } from '../../Field';
import { rgba } from 'emotion-rgba/dist';
import { Button } from '../../Button';
import { isAuth } from '../../../core/helpers';
import { useHistory } from 'react-router-dom';
import { Tariff } from '../OrderForm';
import { FormResponse } from '../../FormResponse';


const OrderCardBase: FC<Props> = ({ className, order, ...rest }: Props) => {
  const history = useHistory();
  const [tariffs, setTariffs] = useState<Tariff[]>();
  const [error, setError] = useState<string>();

  useEffect(() => {
    apiConnect.get('/tariff/list')
      .then(response => {
        setTariffs(response.data.body.tariffs)
      })
  }, [setTariffs])

  const parseTariffs = (tariffsStr: string) => {
    return tariffsStr.split(',').map((n) => {
      return tariffs?.filter(m => Number(n) == m.id)[0].name
    }).join(', ')
  }

  const startOrder = () => {
    apiConnect.get(`/driver/start?token=${isAuth}&order_id=${order.id}`)
      .then(response => {
        if(response.data.body && response.data.body.order){
          localStorage.current_order = JSON.stringify(response.data.body);
          history.push('/');
        }
        if(response.data.error && response.data.error.message === 'balance error') {
          setError('Недостаточно средств на балансе');
        }
      })
  };
  return (
    <div className={classNames(className, 'p-3')} {...rest}>
      <h1 css={{ fontSize: 50 }}><TextInteractive>{order.cost}<span css={{ paddingLeft: 5, fontSize: 22, color: rgba("#282828", 0.7) }}>₸</span></TextInteractive></h1>
      {error && <FormResponse>{error}</FormResponse>}
      <div className="d-flex align-items-center mt-3 mb-2">
        <span className="d-block mr-3">От:</span>
        <Field className="d-block w-100 p-3" value={order.from_address} disabled />
      </div>
      <div className="d-flex align-items-center mb-2">
        <span className="d-block mr-3">До:</span>
        <Field className="d-block w-100 p-3" value={order.to_address} disabled />
      </div>
      {order.tariffs &&
        <div css={{
          fontSize: 19,
          fontWeight: 500,
          marginTop: 20
        }}>{parseTariffs(order.tariffs)}</div>
      }
      <Button className="d-block w-100 mt-4 mb-2 p-3" onClick={startOrder}>Начать выполнение</Button>
    </div>
  );
}

export const OrderCard = styled(OrderCardBase)<Props>`
  border: 1px solid ${Colors.Grey};
  background: #fff;
  border-radius: 4px;
`;
