import styled from '@emotion/styled';
import { Colors } from '../../core';
import { rgba } from 'emotion-rgba/dist';

export const FormResponse = styled.div`
  padding: 10px;
  text-align: center;
  font-weight: 500;
  line-height: 1.4;
  background: ${rgba(Colors.Primary, 0.4)};
`;
