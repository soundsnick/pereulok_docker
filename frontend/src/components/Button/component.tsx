import styled from '@emotion/styled';
import { Colors } from '../../core';

export const Button = styled.button`
  background: ${Colors.Primary};
  border-radius: 4px;
  padding: 10px;
  font-size: 14px;
  border: 0;
  font-weight: 500;
  font-family: inherit;
  outline: none;
  cursor: pointer;
  transition: background 0.2s;
  &:hover {
    background: ${Colors.PrimaryDarker};
  }
  &:active {
    background: ${Colors.PrimaryDark};
  }
`;
