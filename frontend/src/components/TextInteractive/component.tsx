import styled from '@emotion/styled';
import { ColorsText } from '../../core';

export const TextInteractive = styled.span`
  color: ${ColorsText.Primary};
  transition: background 0.2s;
  &:hover {
    color: ${ColorsText.PrimaryDark};
  }
`;
