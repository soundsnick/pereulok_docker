import styled from "@emotion/styled";
import { Props } from './props';
import { css } from '@emotion/core';

export const Container = styled.div<Props>`
  position: relative;
  margin: auto;
  ${({ mini }) => css`
    max-width: ${ mini ? 400 : 1200}px;
  `}
`;
