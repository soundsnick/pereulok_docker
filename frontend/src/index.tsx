import React from 'react';
import ReactDOM from 'react-dom';
import './assets/stylesheets/fonts.css';
import './assets/stylesheets/main.css';
import 'bootstrap-4-grid';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorkerRegistration.register();