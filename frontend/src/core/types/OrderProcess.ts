export type OrderProcess = {
  created_at?: string;
  end_time?: string;
  finished?: number;
  id?: number;
  order_id?: number;
  start_time?: string;
  updated_at?: string;
  user_id?: number;
}
