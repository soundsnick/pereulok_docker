export type Order = {
  readonly cost?: number;
  readonly created_at?: string;
  readonly finished?: number
  readonly from_address?: string;
  readonly from_coords?: string;
  readonly id?: number;
  readonly order_time?: string;
  readonly started?: number;
  readonly to_address?: string;
  readonly to_coords?: string;
  readonly updated_at?: string;
  readonly user_id?: number;
  readonly tariffs?: string;
}
