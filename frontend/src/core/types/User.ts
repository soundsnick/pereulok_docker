export type User = {
  readonly email?: string;
  readonly is_driver?: number;
  readonly name?: string;
  readonly phone?: string;
  readonly id?: number;
  readonly car?: string;
  readonly car_number?: string;
  readonly balance?: number;
}
