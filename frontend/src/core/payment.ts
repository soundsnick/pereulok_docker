import md5 from 'md5';

export const PAYMENT_DEFAULT = {
  pg_merchant_id: 536980,
  pg_description: 'Пополнение баланса',
  pg_success_url: 'http://jolai.kz/balance-success',
  pg_failure_url: 'http://jolai.kz/balance-failure',
  pg_currency: 'KZT'
}

export type Payment = {
  pg_merchant_id: number;
  pg_description: string;
  pg_order_id: string;
  pg_amount: number;
  pg_salt: string;
  pg_currency?: string;
  pg_success_url?: string;
  pg_failure_url?: string;
}

export type PaymentRequest = Payment & {
  pg_sig: string;
}
