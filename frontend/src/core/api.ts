import axios from "axios";

export const apiConnect = axios.create({
  baseURL: "https://api.jolai.kz/",
  responseType: "json"
});

export const payboxConnect = axios.create({
  baseURL: "https://api.paybox.money/",
})
