export enum Colors {
  Primary = "#ffd641",
  PrimaryDarker = "#ffc900",
  PrimaryDark = "#e6aa00",
  Grey = "#ececec"
}

export enum ColorsText {
  Primary = "#de8804",
  PrimaryDark = "#b56e01"
}
