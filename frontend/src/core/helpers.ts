import { ChangeEvent } from 'react';

export const isAuth = localStorage.access_token;
export const handleInputChange = (setter: (newState: string) => void, additionalFunc?: () => void) => (event: ChangeEvent<HTMLInputElement>) => {
  event.target ? setter(event.target.value) : void 0;
  additionalFunc?.();
}

export const handleNumberInputChange = (setter: (newState: number) => void, additionalFunc?: () => void) => (event: ChangeEvent<HTMLInputElement>) => {
  event.target ? setter(Number(event.target.value)) : void 0;
  additionalFunc?.();
}

export const formatDate = (date: number) => String(date).length === 1 ? `0${date}` : date;
