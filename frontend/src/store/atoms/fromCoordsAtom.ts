import { declareAction, declareAtom } from '@reatom/core';

export const setFromCoordsAction = declareAction<Array<number>>();
export const fromCoordsAtom = declareAtom([0, 0], on => [on(setFromCoordsAction, (state, payload) => payload)]);
