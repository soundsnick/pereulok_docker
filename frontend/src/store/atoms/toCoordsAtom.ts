import { declareAction, declareAtom } from '@reatom/core';

export const setToCoordsAction = declareAction<Array<number>>();
export const toCoordsAtom = declareAtom([0, 0], on => [on(setToCoordsAction, (state, payload) => payload)]);
