import { declareAction, declareAtom } from '@reatom/core';

export const authRespond = declareAction<string>();
export const authAtom = declareAtom('', on => [on(authRespond, (state, payload) => payload)]);
