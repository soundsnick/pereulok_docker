import { declareAction, declareAtom } from '@reatom/core';

export const setToAddressAction = declareAction<string>();
export const toAddressAtom = declareAtom('', on => [on(setToAddressAction, (state, payload) => payload)]);
