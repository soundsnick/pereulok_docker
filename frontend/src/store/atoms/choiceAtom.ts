import { declareAction, declareAtom } from '@reatom/core';

export const setChoiceAction = declareAction<boolean>();
export const setChoiceExpAction = declareAction<boolean>();
export const choiceAtom = declareAtom(false, on => [
  on(setChoiceAction, (state) => !state),
  on(setChoiceExpAction, (state, payload) => payload),
]);
