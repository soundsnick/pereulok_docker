import { declareAction, declareAtom } from '@reatom/core';

export const setFromAddressAction = declareAction<string>();
export const fromAddressAtom = declareAtom('', on => [on(setFromAddressAction, (state, payload) => payload)]);
