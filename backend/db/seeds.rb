# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
heavy = Tariff.new
heavy.name = "Грузовой"
heavy.description = "Грузовая машина удобна если вам надо перевезти большой груз"
heavy.save

special = Tariff.new
special.name = "Спец.техника"
special.description = "Если вам нужна специальная техника"
special.save

evacuator = Tariff.new
evacuator.name = "Эвакуатор"
evacuator.description = "Для перевозки техники не на ходу"
evacuator.save

notdrunk = Tariff.new
notdrunk.name = "Трезвый водитель"
notdrunk.description = "Если вам нужен трезвый водитель"
notdrunk.save

city = Tariff.new
city.name = "Город"
city.description = "Для поездок внутри города"
city.save

town = Tariff.new
town.name = "Районы"
town.description = "Для поездок в районах"
town.save

withincity = Tariff.new
withincity.name = "Межгород"
withincity.description = "Для междугородних поездок"
withincity.save