class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.string :street
      t.string :house
      t.string :coordinates
      t.datetime :order_time
      
      t.timestamps
    end
  end
end
