class FinishedColumn < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :finished, :integer
    add_column :order_processes, :finished, :integer
  end
end
