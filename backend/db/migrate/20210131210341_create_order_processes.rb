class CreateOrderProcesses < ActiveRecord::Migration[6.1]
  def change
    create_table :order_processes do |t|
      t.integer :user_id
      t.integer :order_id
      t.datetime :start_time
      t.datetime :end_time
      
      t.timestamps
    end
  end
end
