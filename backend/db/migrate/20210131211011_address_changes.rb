class AddressChanges < ActiveRecord::Migration[6.1]
  def change
    remove_column :orders, :street
    remove_column :orders, :house
    remove_column :orders, :coordinates

    add_column :orders, :from_address, :string
    add_column :orders, :to_address, :string
    add_column :orders, :from_coords, :string
    add_column :orders, :to_coords, :string
  end
end
