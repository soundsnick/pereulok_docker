class OrderCost < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :cost, :integer, default: 0
  end
end
