class AddTariffToOrder < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :tariff_id, :integer
  end
end
