class DeleteUserTariff < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :tariff_id
    remove_column :orders, :tariff_id

    add_column :orders, :tariffs, :string
  end
end
