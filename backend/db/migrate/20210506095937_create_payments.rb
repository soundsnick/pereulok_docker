class CreatePayments < ActiveRecord::Migration[6.1]
  def change
    create_table :payments do |t|
      t.integer :user_id
      t.string :order_id
      t.integer :amount 
      t.boolean :finished
      t.timestamps
    end
  end
end
