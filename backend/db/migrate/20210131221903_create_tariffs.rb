class CreateTariffs < ActiveRecord::Migration[6.1]
  def change
    create_table :tariffs do |t|
      t.string :name
      t.string :description
      t.string :image
      t.timestamps
    end
  end
end
