class StartProcess < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :started, :integer
  end
end
