class StartProcessDefaults < ActiveRecord::Migration[6.1]
  def change
    change_column :orders, :started, :integer, :default => 0
  end
end
