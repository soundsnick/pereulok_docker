class CamelCaseToSnake < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :isAdmin
    add_column :users, :is_driver, :integer, default: 0
    add_column :users, :is_admin, :integer, default: 0
  end
end
