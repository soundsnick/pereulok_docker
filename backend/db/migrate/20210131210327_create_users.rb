class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :password
      t.integer :isAdmin
      t.integer :group_id
      
      t.timestamps
    end
  end
end
