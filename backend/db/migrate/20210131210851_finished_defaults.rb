class FinishedDefaults < ActiveRecord::Migration[6.1]
  def change
    remove_column :order_processes, :finished
    remove_column :orders, :finished
    add_column :order_processes, :finished, :integer, default: 0
    add_column :orders, :finished, :integer, default: 0
  end
end
