module ApiErrors
  def incorrect_input(input)
    [422, nil,
    {
      id: 1,
      message: "Incorrect input",
      expected: input,
      passed: params.permit(*input)
    }]
  end

  def auth_failed
    [401, nil,
    {
        id: 2,
        message: "Email or password incorrect"
    }]
  end

  def already_exists(email)
    [403, nil,
    {
        id: 3,
        message: "User with email: #{email} already exists"
    }]
  end

  def password_dismatch
    [422, nil,
    {
        id: 4,
        message: "Passwords don't match"
    }]
  end

  def token_auth_failed
    [401, nil,
    {
        id: 5,
        message: "Token is incorrect"
    }]
  end

  def order_error
    [404, nil,
     {
         id: 6,
         message: "Order error"
     }]
  end
end