module ApiResponses
  def api_respond(status, body=nil, error=nil)
    tmp = {
        status: status,
    }
    tmp[:body] = body if body
    tmp[:error] = error if error
    tmp
  end
end