class User < ApplicationRecord
    has_many :tokens
    has_many :order_processes
    has_many :issues
    has_many :payments
end
