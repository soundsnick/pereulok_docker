class UserController < ApplicationController
  protect_from_forgery

  def login
    @response = ""

    if validate_login(params)
      if (user = User.find_by(params.permit(:email, :password)))
        token = Token.new
        token.user = user
        token.token = generate_token
        token.save
        @response = [
            200,
            {
                email: user.email,
                token: token.token
            }
        ]
      else
        @response = auth_failed
      end
    else
      @response = incorrect_input(%w(email password))
    end
    render json: api_respond(*@response)
  end

  def register
    @response = ""
    if validate_register(params)
      if validate_password(params)
        if not User.where(email: params[:email]).exists?
          user = User.new(params.permit(:email, :name, :password, :phone))
          user.save
          token = Token.new
          token.user = user
          token.token = generate_token
          token.save
          @response = [
              200,
              {
                  email: user.email,
                  token: token.token
              }
          ]
        else
          @response = already_exists(params[:email])
        end
      else
        @response = password_dismatch
      end
    else
      @response = incorrect_input(%w(email name password password_confirm group_id))
    end
    render json: api_respond(*@response)
  end

  def register_driver
    @response = ""
    if validate_register(params) and params[:car] and params[:car_number]
      if validate_password(params)
        if not User.where(email: params[:email]).exists?
          user = User.new(params.permit(:email, :name, :password, :phone, :car, :car_number))
          user.is_driver = 1
          user.save
          token = Token.new
          token.user = user
          token.token = generate_token
          token.save
          @response = [
              200,
              {
                  email: user.email,
                  token: token.token
              }
          ]
        else
          @response = already_exists(params[:email])
        end
      else
        @response = password_dismatch
      end
    else
      @response = incorrect_input(%w(email name password password_confirm car car_number))
    end
    render json: api_respond(*@response)
  end

  def update
    @response = ""
    if validate_update(params)
      if (user = token_auth(params[:token]))
        user.name = params[:name] if params[:name]
        user.phone = params[:phone] if params[:phone]
        user.car = params[:car] if params[:car]
        user.car_number = params[:car_number] if params[:car_number]
      
        user.save
        @response = [
            200,
            {
                user: user
            }
        ]
      else
        @response = auth_failed
      end
    else
      @response = incorrect_input("%w(token)")
    end
  end

  def logout
    if params[:token] and (@token = Token.find_by(token: params[:token]))
      @token.destroy
    end
    render body: api_respond(200)
  end

  def get_id
    @response = ""
    if params[:id]
      if (@user = User.where(id: params[:id]).select("id, name, email, is_driver, phone, car, car_number").first)
        @response = [
            200,
            {
                user: @user
            }
        ]
      else
        @response = [
            404,
            {
                user: nil
            }
        ]
      end
    else
      @response = incorrect_input(%w(id))
    end
    render json: api_respond(*@response)
  end

  def get
    @response = ""
    if params[:email]
      if (@user = User.where(email: params[:email]).select("id, name, email, is_driver, phone, car, car_number").first)
        @response = [
            200,
            {
                user: @user
            }
        ]
      else
        @response = [
            404,
            {
                user: nil
            }
        ]
      end
    else
      @response = incorrect_input(%w(email))
    end
    render json: api_respond(*@response)
  end

  def get_token
    @response = ""
    if params[:token]
      if @user = token_auth(params[:token])
        @response = [
            200,
            {
                user: @user
            }
        ]
      else
        @response = [
            404,
            {
                user: nil
            }
        ]
      end
    else
      @response = incorrect_input(%w(email))
    end
    render json: api_respond(*@response)
  end

  private
  def validate_login(params)
    params[:email] and params[:password] and params[:email].length > 0 and params[:password].length > 0
  end

  def validate_register(params)
    params[:email] and params[:password] and params[:password_confirm] and
    params[:name] and params[:email].length > 0 and params[:password].length > 0 and
    params[:password_confirm].length > 0 and params[:phone]
  end

  def validate_password(params)
    params[:password] == params[:password_confirm]
  end

  def validate_update(params)
    params[:token]
  end

  def generate_token
    loop do
      token = SecureRandom.hex(20)
      break token unless Token.where(token: token).exists?
    end
  end
end