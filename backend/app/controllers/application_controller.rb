class ApplicationController < ActionController::Base
    include ApiErrors, ApiResponses

    def token_auth(token_param)
        token = Token.find_by(token: token_param)
        if token
            User.select(:id, :email, :name, :is_driver, :phone, :car, :car_number, :balance).find_by(id: token.user.id)
        else
            false
        end
    end
end
