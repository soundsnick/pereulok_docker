class PaymentController < ApplicationController
    protect_from_forgery
    require 'uri'
    require 'net/http'
    require 'crack'
    require 'json'
  
    def create 
        @res = { status: 'error' }
        if params[:user_id] and params[:order_id] 
            @payment = Payment.new
            @payment.user_id = params[:user_id]
            @payment.amount = params[:amount]
            @payment.order_id = params[:order_id]
            @payment.finished = false
            @payment.save
            uri = URI("https://api.paybox.money/init_payment.php?#{params[:paybox]}")
            res = Net::HTTP.get_response(uri)
            if res.is_a?(Net::HTTPSuccess)
                parsedJson = Crack::XML.parse(res.body)
                red_url = parsedJson['response']['pg_redirect_url'] if parsedJson['response'] and parsedJson['response']['pg_redirect_url']
                @res = { redirect_url: red_url }
            end
        end
        render json: @res
    end

    def success
        if params[:order_id] and @payment = Payment.find_by(order_id: params[:order_id], finished: false)
            if @user = User.find_by(id: @payment.user_id)
                @user.balance = @user.balance + @payment.amount
                @user.save 
            end
            @payment.finished = true 
            @payment.save 
        end
        render body: 'ok'
    end

    def failure
        if params[:order_id] and @payment = Payment.find_by(order_id: params[:order_id], finished: false)
            @payment.finished = true 
            @payment.save 
        end
        render body: 'ok'
    end
  end