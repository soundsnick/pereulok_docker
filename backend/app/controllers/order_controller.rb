class OrderController < ApplicationController
  protect_from_forgery

  def create
    @response = ""
    if validate_create(params)
      if (user = token_auth(params[:token]))
        order = Order.new
        order.user = user
        order.from_address = params[:from_address]
        order.to_address = params[:to_address]
        order.from_coords = params[:from_coords]
        order.to_coords = params[:to_coords]
        order.cost = params[:cost]
        order.tariffs = params[:tariffs]
        order.order_time = Time.now
        order.save
        @response = [200, { order: order }]
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token from_address to_address from_coords to_coords cost tariffs))
    end
    render json: api_respond(*@response)
  end

  def cancel
    @response = ""
    if validate_cancel(params)
      if(user = token_auth(params[:token]))
        order = Order.where(user_id: user.id).order(id: :desc).take
        order.destroy
        @response = [200, { message: "deleted" }]
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token))
    end
    render json: api_respond(*@response)
  end

  def get
    @response = ""
    if validate_get(params)
      if user = token_auth(params[:token]) and order = Order.find_by(id: params[:order_id])
        response = {
            order: order
        }
        if(order_process = OrderProcess.find_by(order_id: order.id))
            response['process'] = order_process
        end
        @response = [200, response]
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token))
    end
    render json: api_respond(*@response)
  end

  def get_user
    @response = ""
    if validate_cancel(params)
      if user = token_auth(params[:token])
        if(user.is_driver = 0)
          if order = Order.where(user_id: user.id).order(id: :desc).take
            response = {
                order: order
            }
            if(order_process = OrderProcess.find_by(order_id: order.id))
              response['process'] = order_process
            end
            @response = [200, response]
          else
            @response = [404, {}]
          end
        else
          if order = OrderProcess.where(user_id: user.id).order(id: :desc).take.order
            response = {
                order: order
            }
            if(order_process = OrderProcess.find_by(order_id: order.id))
              response['process'] = order_process
            end
            @response = [200, response]
          else
            @response = [404, {}]
          end
        end
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token))
    end
    render json: api_respond(*@response)
  end

  def list
    @response = ""
    if validate_list(params)
      if is_driver(params[:token])
        orders = Order.where(finished: 0, started: 0).order(order_time: :asc)
        @response = [200, { orders: orders }]
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(['token'])
    end
    render json: api_respond(*@response)
  end

  def start
    @response = ""
    if validate_finish(params)
      if is_driver(params[:token])
        if (order = Order.find_by(id: params[:order_id])) and order.finished == 0 and order.started == 0
          user = token_auth(params[:token])
          earn = order.cost * 0.05
          if user.balance > earn
            order_process = OrderProcess.new
            order_process.user = user
            order_process.order = order
            order_process.start_time = Time.now
            order_process.save
            order.started = 1
            user.balance = user.balance - earn
            user.save
            order.save
            @response = [200, {order: order, process: order_process}]
          else
            @response = [422, nil,
                         {
                           message: "balance error"
                         }]
          end
        else
          @response = order_error
        end
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token order_id))
    end
    render json: api_respond(*@response)
  end

  def finish
    @response = ""
    if validate_finish(params)
      if is_driver(params[:token])
        if validate_order(params[:order_id], params[:token])
          user = token_auth(params[:token])
          order_process = OrderProcess.find_by(order_id: params[:order_id], user_id: user.id)
          order_process.end_time = Time.now
          order_process.finished = 1
          order_process.save
          order = order_process.order
          order.finished = 1
          order.save
          @response = [200, {order: order, process: order_process}]
        else
          @response = order_error
        end
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token order_id))
    end
    render json: api_respond(*@response)
  end

  private

  def validate_create(params)
    params[:token] and params[:to_coords] and params[:from_coords] and params[:from_address] and params[:to_address] and params[:cost] and params[:tariffs]
  end

  def validate_cancel(params)
    params[:token]
  end

  def validate_get(params)
    params[:token] and params[:order_id]
  end

  def validate_list(params)
    params[:token]
  end

  def validate_finish(params)
    params[:token] and params[:order_id]
  end

  def is_driver(token_param)
    token = Token.find_by(token: token_param)
    token and token.user.is_driver
  end

  def validate_order(order_id, token_param)
    order_process = OrderProcess.find_by(order_id: order_id)
    order_process and order_process.user == token_auth(token_param) and order_process.order.finished == 0 and order_process.order.started == 1
  end
end