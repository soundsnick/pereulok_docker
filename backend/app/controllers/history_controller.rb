class HistoryController < ApplicationController
  protect_from_forgery

  def all
    @response = ""
    if verify_auth(params) and token_auth(params[:token])
      @response = [200, { orders: Order.where(finished: 1) }]
    else
      @response = token_auth_failed
    end
    render json: api_respond(*@response)
  end

  def user
    @response = ""
    if verify_auth(params) and (user = token_auth(params[:token]))
      @response = [200, { orders: Order.where(user: user) }]
    else
      @response = token_auth_failed
    end
    render json: api_respond(*@response)
  end

  def driver
    @response = ""
    if verify_auth(params) and (user = token_auth(params[:token]))
      if User.find_by(id: user.id).is_driver
        orders = []
        OrderProcess.where(user: user, finished: 1).each do |order_process|
          orders << order_process.order
        end
        @response = [200, { orders: orders }]
      else
        @response = [301, nil, "user is not a driver"]
      end
    else
      @response = token_auth_failed
    end
    render json: api_respond(*@response)
  end

  private
  def verify_auth(params)
    params[:token]
  end
end