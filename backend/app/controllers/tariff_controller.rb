class TariffController < ApplicationController
  def list
    tariffs = Tariff.all.order(id: :asc)
    @response = [200, { tariffs: tariffs }]

    render json: api_respond(*@response)
  end
end