class DocumentationController < ApplicationController
  require 'json'
  protect_from_forgery

  def index
    @models = [
        {
            name: "users",
            path: documentation_users_path,
            methods: [:login, :register, :update, :logout, :get]
        },
        {
            name: "orders",
            path: documentation_orders_path,
            methods: [:create, :start, :finish, :list]
        },
        {
            name: "history",
            path: documentation_history_path,
            methods: [:all, :user, :driver]
        },
        {
            name: "issues",
            path: documentation_issues_path,
            methods: [:create, :delete, :user]
        }
    ]
    render 'documentation/index'
  end

  def users
    @model = model_template("User", "User authentication/registration", [
        method_template(
            "login", "Authentication", user_login_path, 1,
            [:email, :password],
            {
                email: "string",
                password: "string"
            },
            {
                email: "soundsnick@gmail.com",
                token: "string"
            }
        ),
        method_template(
            "register", "Registration", user_register_path, 0,
            [:email, :name, :password, :password_confirm],
            {
                email: "soundsnick@gmail.com",
                name: "string",
                password: "string",
                password_confirm: "string"
            },
            {
                email: "soundsnick@gmail.com",
                token: "string"
            }
        ),
        method_template(
            "update", "Profile update", user_update_path, 1,
            [:token, :email?, :name],
            {
                token: "string",
                email: "soundsnick@gmail.com"
            },
            {
                user: "UserObject"
            }
        ),
        method_template(
            "logout", "Logout", user_logout_path, 0,
            [:token],
            {
                token: "string"
            }
        ),
        method_template(
            "get", "Get User", user_get_path, 0,
            [:email],
            {
                email: "soundsnick@gmail.com"
            },
            {
                user: "UserObject"
            }
        )
    ])
    render 'documentation/documentation'
  end

  def orders
    @model = model_template("Order", "Order business logic", [
        method_template(
            "create", "Order create", order_create_path, 1,
            [:token, :cost, :to_address, :from_address, :to_coords, :from_coords],
            {
                token: "string",
                cost: 300,
                to_address: "string",
                from_address: "string",
                to_coords: "string",
                from_coords: "string"
            },
            {
                order: "OrderObject"
            }
        ),
        method_template(
            "start", "Driver's order start", order_start_path, 0,
            [:token, :order_id],
            {
                token: "string",
                order_id: "integer"
            },
            {
                order: "OrderObject",
                process: "OrderProcessObject"
            }
        ),
        method_template("finish", "Driver's order finish", order_finish_path, 0,
            [:token, :order_id],
            {
                token: "string",
                order_id: "integer"
            },
            {
                order: "OrderObject",
                process: "OrderProcessObject"
            }
        ),
        method_template(
            "list", "Created orders list", order_list_path, 0,
            [:token],
            {
                token: "string"
            },
            {
                orders: "OrderObjectList"
            }
        )
    ])
    render 'documentation/documentation'
  end

  def history
    @model = model_template(
        "History", "Order history", [
        method_template(
            "all", "List all finished orders", history_all_path, 0,
            [:token],
            {
                token: "string"
            },
            {
                orders: "OrderObjectList"
            }
        ),
        method_template(
            "user", "List all finished orders of the user", history_user_path, 0,
            [:token],
            {
                token: "string"
            },
            {
                orders: "OrderObjectList"
            }
        ),
        method_template(
            "driver", "List all finished orders of a driver", history_driver_path, 0,
            [:token],
            {
                token: "string"
            },
            {
                orders: "OrderObjectList"
            }
        )
    ])
    render 'documentation/documentation'
  end

  def issues
    @model = model_template("Issues", "Issues from users", [
        method_template(
            "create", "Create an issue", issue_create_path, 1,
            [:token, :title, :content],
            {
                token: "string",
                title: "string",
                content: "text"
            },
            {
                issue: "IssueObject"
            }
        ),
        method_template(
            "delete", "Delete an issue", issue_delete_path, 0,
            [:token, :id],
            {
                token: "string",
                id: "issue id",
            }
        ),
        method_template(
            "user", "Get user's issues list", issue_user_path, 0,
            [:token],
            {
                token: "string"
            },
            {
                issues: "IssueObjectList"
            }
        )
    ])
    render 'documentation/documentation'
  end

  private
  def model_template(name, description, methods)
    {
        name: name,
        description: description,
        methods: methods
    }
  end

  def method_template(name, description, path, http_method, params, input, output = nil)
    {
        name: name,
        description: description,
        path: path,
        http_method: %w[GET POST][http_method],
        params: params,
        input: JSON.pretty_generate(input),
        output: JSON.pretty_generate(api_respond(200, output))
    }
  end
end