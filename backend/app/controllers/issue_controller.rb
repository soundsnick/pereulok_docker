class IssueController < ApplicationController
  protect_from_forgery

  def create
    @response = ""
    if validate_create(params)
      if (user = token_auth(params[:token]))
        issue = Issue.new(params.permit(:title, :content))
        issue.user_id = user.id
        issue.save
        @response = [200, { issue: issue }]
      else
        @response = token_auth_failed
      end
    else
      @response = incorrect_input(%w(token title content))
    end
    render json: api_respond(*@response)
  end

  def delete
    @response = ""
    if validate_delete(params) and (user = token_auth(params[:token])) and (issue = Issue.find_by(id: params[:id])) and issue.user.id == user.id
      issue.destroy
      @response = [200]
    else
      @response = token_auth_failed
    end
    render json: api_respond(*@response)
  end

  def user
    @response = ""
    if params[:token] and (user = token_auth(params[:token]))
      @response = [200, { issues: Issue.where(user_id: user.id) }]
    else
      @response = token_auth_failed
    end
    render json: api_respond(*@response)
  end

  private
  def validate_create(params)
    params[:token] and params[:title] and params[:content]
  end

  def validate_delete(params)
    params[:token] and params[:id]
  end
end