Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  # Authentication
  post 'user/login', to: 'user#login', as: :user_login
  get 'user/logout', to: 'user#logout', as: :user_logout
  post 'user/register', to: 'user#register', as: :user_register
  post 'user/driver/register', to: 'user#register_driver', as: :user_register_driver
  get 'user/get', to: 'user#get', as: :user_get
  get 'user/get/id', to: 'user#get_id', as: :user_get_id
  get 'user/get/token', to: 'user#get_token', as: :user_get_token
  post 'user/update', to: 'user#update', as: :user_update

  # Orders/OrderProcesses
  get 'order/list', to: 'order#list', as: :order_list
  post 'order/create', to: 'order#create', as: :order_create
  post 'order/cancel', to: 'order#cancel', as: :order_cancel
  get 'order/get', to: 'order#get', as: :order_get
  get 'order/get/user', to: 'order#get_user', as: :order_get_user
  get 'driver/start', to: 'order#start', as: :order_start
  get 'driver/finish', to: 'order#finish', as: :order_finish

  # Order History
  get 'history/all', to: 'history#all', as: :history_all
  get 'history/user', to: 'history#user', as: :history_user
  get 'history/driver', to: 'history#driver', as: :history_driver

  # Issues
  get 'issue/create', to: 'issue#create', as: :issue_create
  get 'issue/delete', to: 'issue#delete', as: :issue_delete
  get 'issue/user', to: 'issue#user', as: :issue_user

  # Tariffs
  get 'tariff/list', to: 'tariff#list', as: :tariff_list

  # Payment processing
  post 'payment/create', to: 'payment#create', as: :payment_create
  post 'payment/success', to: 'payment#success', as: :payment_success
  post 'payment/failure', to: 'payment#failure', as: :payment_failure

  # Api Docs
  get 'docs', to: 'documentation#index', as: :documentation
  get 'docs/users', to: 'documentation#users', as: :documentation_users
  get 'docs/orders', to: 'documentation#orders', as: :documentation_orders
  get 'docs/history', to: 'documentation#history', as: :documentation_history
  get 'docs/issues', to: 'documentation#issues', as: :documentation_issues


end
